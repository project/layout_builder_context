<?php

namespace Drupal\Tests\layout_builder_context\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the Visibility service.
 *
 * @package Drupal\Tests\layout_builder_context\Kernel
 *
 * @group layout_builder_context
 */
class VisibilityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'context',
    'layout_builder_context',
    'layout_builder_context_test',
  ];

  /**
   * The Visibility service.
   *
   * @var \Drupal\layout_builder_context\Utility\Visibility
   */
  protected $visibility;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->visibility = $this->container->get('layout_builder_context.visibility');
    $this->installConfig('layout_builder_context_test');
  }

  /**
   * Tests that render arrays are modified as expected.
   */
  public function testRenderArrayIsModified() {
    $build = [];
    $build['content'] = ['foo'];
    $contexts = [
      'foo',
      'bar',
      'baz',
    ];

    $build = $this->visibility->evaluate($build, $contexts, TRUE);
    $this->assertNotEmpty($build['content']);

    // The assigned Contexts should be listed in the render array
    $this->assertEquals($contexts, $build['#context_visibility']);

    $build = [];
    $build['content'] = ['foo'];
    $contexts = [
      'foo',
      'bar',
      'disabled',
    ];

    $build = $this->visibility->evaluate($build, $contexts, TRUE);

    // A disabled context should have cache tags applied
    $this->assertTrue(in_array('config:context.context.disabled', $build['#cache']['tags']));

    $build = [];
    $build['content'] = ['foo'];
    $contexts = [
      'foo',
      'bar',
      'disabled',
      'fake'
    ];

    // Non-existent contexts should be skipped
    $build = $this->visibility->evaluate($build, $contexts, TRUE);
    $this->assertEquals(['foo', 'bar', 'disabled'], $build['#context_visibility']);

    $build = [];
    $build['content'] = ['foo'];
    $contexts = [
      'foo',
      'bar',
      'foo_404',
    ];

    // foo_404 should evaluate to FALSE, so content array is removed.
    $build = $this->visibility->evaluate($build, $contexts, TRUE);
    $this->assertArrayHasKey('content', $build);
    $this->assertArrayHasKey('#access', $build['content']);
    $this->assertFalse($build['content']['#access']);

    // foo_404 should evaluate to FALSE, but all must pass is FALSE so 'content' should remain
    $build['content'] = ['foo'];
    $build = $this->visibility->evaluate($build, $contexts, FALSE);
    $this->assertArrayHasKey('content', $build);
  }

  /**
   * Tests that the context options function returns only active Contexts.
   */
  public function testContextOptionsList() {
    $contexts = layout_builder_context_option_list();
    $this->assertCount(4, $contexts);
  }

}
