# Layout Builder Context

Layout Builder Context adds the capability for layout sections and block
components for Layout Builder to leverage the Context module to assign
visibility to either an entire layout or individual components within them.

This makes it possible to hide all or parts of areas with Layout Builder
with any Contexts you come up with. This is intended to drive visibility
only. Contexts with Reactions won't have any effect.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/layout_builder_context).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/layout_builder_context).

## Table of contents

- Requirements
- Installation
- Configuration

## Requirements

This module requires the core Layout Builder and contributed Context module.

## Installation

Install this module like any other normal Drupal module.

## Configuration

You will now see 'Context visibility' option on layouts and blocks for use
in layouts.

Create some Contexts at Admin > Structure > Context and start assigning them
accordingly. If the Context condition(s) do not pass, the layout or block
will not be rendered to the user.
