<?php

namespace Drupal\layout_builder_context\Utility;

use Drupal\context\ContextManager;
use Drupal\Core\Cache\Cache;
use Drupal\context\Entity\Context;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Help evaluate Context visibility when rendering layouts or components.
 */
class Visibility {

  /**
   * The context manager service.
   *
   * @var \Drupal\context\ContextManager
   */
  protected $contextManager;

  /**
   * Visibility class constructor.
   *
   * @param \Drupal\context\ContextManager $context_manager
   *   The context manager service.
   */
  public function __construct(ContextManager $context_manager) {
    $this->contextManager = $context_manager;
  }

  /**
   * Helper to evaluate Context visibility and modify the render array.
   *
   * @param array $build
   *   The render array.
   * @param array $contexts
   *   The Contexts applied to a layout or component.
   * @param bool $all_must_pass
   *   Whether all Contexts must pass for this renderable item or not.
   *
   * @return array
   *   The modified render array.
   */
  public function evaluate(array $build, array $contexts, bool $all_must_pass) {
    if (!isset($build['#context_visibility'])) {
      $build['#context_visibility'] = [];
    }

    /** @var \Drupal\context\Entity\Context $context */
    foreach ($contexts as $context_id) {
      $context = $this->contextManager->getContext($context_id);

      if ($context instanceof Context) {
        $build['#context_visibility'][] = $context_id;
        $visible = !(!$context->disabled()) || $this->contextManager->evaluateContextConditions($context);
        $cache_contexts = $context->getCacheContexts();
        $cache_tags = $context->getCacheTags();

        if (!empty($cache_contexts)) {
          $existing_cache_contexts = $build['#cache']['contexts'] ?? [];
          $build['#cache']['contexts'] = Cache::mergeContexts($existing_cache_contexts, $cache_contexts);
        }

        if (!empty($cache_tags)) {
          $existing_cache_tags = $build['#cache']['tags'] ?? [];
          $build['#cache']['tags'] = Cache::mergeTags($existing_cache_tags, $cache_tags);
        }

        if (!empty($build['content'])) {
          CacheableMetadata::createFromRenderArray($build)
            ->merge(CacheableMetadata::createFromRenderArray($build['content']))
            ->applyTo($build);
        }

        if (!$visible && $all_must_pass) {
          $build['content'] = [];
        }
      }
    }

    return $build;
  }

}
