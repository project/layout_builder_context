<?php

namespace Drupal\layout_builder_context\EventSubscriber;

use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder_context\Utility\Visibility;

/**
 * Event subscriber to the initial render array for Layout Builder.
 */
class BlockComponentRenderArraySubscriber implements EventSubscriberInterface {

  /**
   * The visibility service.
   *
   * @var \Drupal\layout_builder_context\Utility\Visibility
   */
  protected $visibility;

  /**
   * BlockComponentRenderArraySubscriber constructor.
   *
   * @param \Drupal\layout_builder_context\Utility\Visibility $visibility
   *   The visibility service.
   */
  public function __construct(Visibility $visibility) {
    $this->visibility = $visibility;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'onBuildRender',
      50,
    ];
    return $events;
  }

  /**
   * Check if the block should render or not.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $build = $event->getBuild();

    if (empty($build) || $event->inPreview()) {
      return;
    }

    $contexts = $event->getComponent()->get('context_visibility') ?? [];
    $rule = $event->getComponent()->get('context_all_must_pass') ?? TRUE;

    if (!empty($contexts) && is_array($contexts)) {
      $build = $this->visibility->evaluate($build, $contexts, $rule);
    }

    $event->setBuild($build);
  }

}
